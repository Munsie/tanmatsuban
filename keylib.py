import msvcrt

def check():
    if msvcrt.kbhit():
        ch = ord(msvcrt.getch())
        if ch == 224: #Arrow key
            ch = ord(msvcrt.getch())
            if ch == 72:
                return "KEY_UP"
            elif ch == 75:
                return "KEY_LEFT"
            elif ch == 77:
                return "KEY_RIGHT"
            elif ch == 80:
                return "KEY_DOWN"
        elif ch == 27:
            return "KEY_ESC"
        elif ch == 8:
            return "KEY_BACK"
        elif ch == 9:
            return "KEY_TAB"
        elif ch == 13:
            return "KEY_ENTER"
        elif ch == 32:
            return "KEY_SPACE"
        elif ch >= 48 and ch <= 57:
            return "KEY_"+chr(ch)
        elif ch >= 65 and ch <= 90:
            return "KEY_"+chr(ch)
        elif ch >= 97 and ch <= 122:
            return "KEY_"+chr(ch-32)
        else:
            return None