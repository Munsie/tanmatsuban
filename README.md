Tanmatsuban (端末番) is a light-weight Sokoban-inspired puzzle game with 45
puzzles to solve. Levels ramp up in difficulty and complexity as you progress
and introduce new mechanics and block types along the way. Do you have what it
takes to solve them all?

Past and current builds can be found in the 'Builds' directory of this
repository. Alternatively, you can check it out at: https://munsie.itch.io/tanmatsuban