#端末番 (Tanmatsuban) by Jake Munsie

import os
import keylib
import gfx
import tanmatsuban_game

def load_save():
    if not os.path.exists("tanmatsuban.txt"):
        file = open("tanmatsuban.txt", "w+")
        for i in range(45):
            file.write("0")
        file.close()
    
    with open("tanmatsuban.txt", "r") as file:
        file_string = file.read()
        data = []
        for i in range(len(file_string)):
            data.append(file_string[i])
    file.close()
    return data

def save_data(data):
    file = open("tanmatsuban.txt", "w+")
    for i in range(len(data)):
        file.write(data[i])
    file.close()
    
def draw():
    gfx.clear_pixels(pixels)
    gfx.draw_sprite(pixels, 2 + cursor_x * 3, 2 + cursor_y * 3, ["┌──┐", "│  │", "│  │", "└──┘"])
    gfx.draw_border(pixels, 0, 0, 49, 22)
    for i in range(3):
        for j in range(15):
            x_pos = 3 + j * 3
            y_pos = 3 + i * 3
            num = j + i * 15
            gfx.draw_text(pixels, x_pos, y_pos, str(num).zfill(2))
            if player_data[num] == "1":
                gfx.draw_text(pixels, x_pos, y_pos+1, "✓")
    #Controls section
    gfx.draw_text(pixels, 7, 13, "Controls:")
    gfx.draw_text(pixels, 4, 15, "Move      ←↓↑→")
    gfx.draw_text(pixels, 4, 16, "Select   ENTER")
    gfx.draw_text(pixels, 4, 17, "Reset        R")
    gfx.draw_text(pixels, 4, 18, "Undo      BACK")
    gfx.draw_text(pixels, 4, 19, "Quit       ESC")
                
    #Tutorial section
    gfx.draw_text(pixels, 33, 13, "Objective:")
    gfx.draw_sprite(pixels, 29, 14, ["┌ ┐", " ¤ ", "└ ┘"])
    gfx.draw_text(pixels, 33, 15, "->")
    gfx.draw_sprite(pixels, 36, 14, ["┌─┐", "│A│", "└─┘"])
    gfx.draw_text(pixels, 40, 15, "->")
    gfx.draw_sprite(pixels, 43, 14, ["· ·", " A ", "· ·"])
    gfx.draw_text(pixels, 30, 18, "Push the BLOCKS")    
    gfx.draw_text(pixels, 31, 19, "onto BUTTONS")
    gfx.draw_text(pixels, 3, 0, " Tanmatsuban ")
    gfx.draw_text(pixels, 34, 22, " Jake Munsie ")
    gfx.print_display(pixels)
    
cursor_x = 0
cursor_y = 0    
player_data = load_save()
pixels = gfx.create_pixels(50, 23)
key = None

draw()

while True:
    key = keylib.check()
    
    if key == "KEY_LEFT" or key == "KEY_A":
        cursor_x -= 1
    elif key == "KEY_RIGHT" or key == "KEY_D":
        cursor_x += 1
    elif key == "KEY_UP" or key == "KEY_W":
        cursor_y -= 1
    elif key == "KEY_DOWN" or key == "KEY_S":
        cursor_y += 1
    elif key == "KEY_ENTER":
        level = cursor_y * 15 + cursor_x
        level_name = str(level).zfill(2)
        if tanmatsuban_game.play_game(level_name):
            player_data[level] = "1"
            save_data(player_data)
    elif key == "KEY_ESC":
        break
    
    #Clamp cursor
    cursor_x = max(0, min(cursor_x, 14))
    cursor_y = max(0, min(cursor_y, 2))
    
    if key != None:
        draw()
        key = None
        
print("Thanks for playing!")