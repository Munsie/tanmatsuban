import os

#·
nine_slice = [['╔', '═', '╗'], ['║', ' ', '║'], ['╚', '═', '╝']]
junctions = ['╠', '╣', '╦', '╩', '╬']

#Initialize the 'array' for drawing
#48 width, 23 height for "full screen"
def create_pixels(width, height):
    pixels = [[" " for x in range(width)] for y in range(height)]
    return pixels

#Erases all the cells and replaces with a blank/filler char. Does not clear the console window.
def clear_pixels(pixels):
    height = len(pixels)
    width = len(pixels[0])
    for y in range(height):
        for x in range(width):
            pixels[y][x] = " "

#Used to define the screen position of a single ASCII character
def draw_pixel(pixels, x, y, char):
    pixels[y][x] = char

#Draw a single line of text onto the chosen screen location    
def draw_text(pixels, x, y, string):
    for i in range(len(string)):
        pixels[y][x+i] = string[i]

#Draw a "sprite", stored as a 2D list of chars, onto the chosen screen location        
def draw_sprite(pixels, x, y, sprite):
    for i in range(len(sprite)):
        for j in range(len(sprite[0])):
            pixels[y+i][x+j] = sprite[i][j]

#Pass in the x, y coordinates of the top left corner along with the width and height.    
def draw_border(pixels, x1, y1, width, height):
    x2 = x1+width
    y2 = y1+height
    
    #Set corners of border
    pixels[y1][x1] = nine_slice[0][0] #Top left
    pixels[y1][x2] = nine_slice[0][2] #Top right
    pixels[y2][x1] = nine_slice[2][0] #Bottom left
    pixels[y2][x2] = nine_slice[2][2] #Bottom right
    
    #Set edges of border
    for x in range(x1+1, x2):
        pixels[y1][x] = nine_slice[0][1] #Top
        pixels[y2][x] = nine_slice[2][1] #Bottom
    for y in range(y1+1, y2):
        pixels[y][x1] = nine_slice[1][0] #Left
        pixels[y][x2] = nine_slice[1][2] #Right

#Wipes console window and pushes current pixels to screen
def print_display(pixels):
    os.system('cls' if os.name == 'nt' else 'clear')
    
    height = len(pixels)
    width = len(pixels[0])
    for y in range(height):
        for x in range(width):
            print(pixels[y][x], end='')
        print('')