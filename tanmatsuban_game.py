#端末番 (Tanmatsuban) by Jake Munsie

import keylib
import gfx
import tanmatsuban_levels
import time

class Player():
    def __init__(self, x, y):
        self.name = "player"
        self.x = x
        self.y = y
        self.sprite = ["┌ ┐", " ¤ ", "└ ┘"]
    def move(self, x2, y2, objects):
        valid_move = True
        for object in objects: #Loop through every game object on the board
            if object.x == self.x + x2 and object.y == self.y + y2: #Check if the object is in your path...
                if object.name == "wall": #Moving into a wall is invalid
                    valid_move = False                
                elif object.name in ["block", "countdown", "slider"]: #Moving into a block *can* be valid...
                    if object.move(x2, y2, objects) == True:
                        pass
                    else:
                        valid_move = False

        if valid_move:
            self.x += x2
            self.y += y2
        
        #Lock movement to board
        self.x = max(0, min(self.x, 15))
        self.y = max(0, min(self.y, 6))

class Block():
    def __init__(self, x, y, letter):
        self.name = "block"
        self.x = x
        self.y = y
        self.letter = letter
        self.sprite = ["┌─┐", "│" + self.letter + "│", "└─┘"]
    def move(self, x2, y2, objects):
        valid_move = True
        for object in objects: #Loop through every game object on the board
            if object.x == self.x + x2 and object.y == self.y + y2: #Check if the object is the block's path
                if object.name in ["wall", "block", "countdown", "slider", "phaser"]:
                    valid_move = False                

        if valid_move:
            self.x += x2
            self.y += y2
            
        #Lock movement to board
        if self.x < 0 or self.x > 15 or self.y < 0 or self.y > 6:
            self.x = max(0, min(self.x, 15))
            self.y = max(0, min(self.y, 6))
            valid_move = False
        
        return True if valid_move else False

class Countdown():
    def __init__(self, x, y, count):
        self.name = "countdown"
        self.x = x
        self.y = y
        self.count = count
        self.sprite = ["┌─┐", "│" + str(self.count) + "│", "└─┘"]
        
    def move(self, x2, y2, objects):
        valid_move = True
        for object in objects: #Loop through every game object on the board
            if object.x == self.x + x2 and object.y == self.y + y2: #Check if the object is the block's path
                if object.name in ["wall", "block", "countdown", "slider", "phaser"]:
                    valid_move = False                

        if self.count <= 0:
            valid_move = False
        
        if valid_move:
            self.x += x2
            self.y += y2
            self.count -= 1
            self.sprite = ["┌─┐", "│" + str(self.count) + "│", "└─┘"]
            
        #Lock movement to board
        if self.x < 0 or self.x > 15 or self.y < 0 or self.y > 6:
            self.x = max(0, min(self.x, 15))
            self.y = max(0, min(self.y, 6))
            valid_move = False
        
        return True if valid_move else False

class Slider():
    def __init__(self, x, y, letter):
        self.name = "slider"
        self.x = x
        self.y = y
        self.letter = letter
        self.sprite = ["┌─┐", "│" + self.letter + "│", "└─┘"]
        
    def move(self, x2, y2, objects):
        valid_move = True
        for object in objects:
            if object.x == self.x + x2 and object.y == self.y + y2:
                if object.name in ["wall", "block", "countdown", "slider", "phaser"]:
                    valid_move = False

        if self.letter == "↔" and y2 != 0:
            valid_move = False
        if self.letter == "↕" and x2 != 0:
            valid_move = False

        if valid_move:
            self.x += x2
            self.y += y2
            
        #Lock movement to board
        if self.x < 0 or self.x > 15 or self.y < 0 or self.y > 6:
            self.x = max(0, min(self.x, 15))
            self.y = max(0, min(self.y, 6))
            valid_move = False
        
        return True if valid_move else False

class Phaser():
    def __init__(self, x, y):
        self.name = "phaser"
        self.x = x
        self.y = y
        self.sprite = ["┌ ┐", " ≈ ", "└ ┘"]
        
class Button():
    def __init__(self, x, y, letter):
        self.name = "button"
        self.x = x
        self.y = y
        self.letter = letter
        self.sprite = ["· ·", " " + self.letter + " ", "· ·"]
        
class Wall():
    def __init__(self, x, y):
        self.name = "wall"
        self.x = x
        self.y = y
        self.sprite = ["███", "███", "███"]

def draw(pixels, objects, level):
    gfx.clear_pixels(pixels)
    for object in objects:
        gfx.draw_sprite(pixels, 1 + object.x * 3, 1 + object.y * 3, object.sprite)
    gfx.draw_border(pixels, 0, 0, 49, 22)
    if level in ["00", "01", "02"]:
        gfx.draw_text(pixels, 3, 21, "R to Reset")
        gfx.draw_text(pixels, 30, 21, "Backspace to Undo")  
    gfx.draw_text(pixels, 3, 0, " Level " + level + " ")
    gfx.print_display(pixels)

def check_solution(objects):    
    correct = 0
    total = 0

    for object in objects:
        if object.name == "button":
            total += 1
            for obj in objects:
                if obj.name == "block":
                    if object.x == obj.x and object.y == obj.y and object.letter == obj.letter:
                        correct += 1
    
    if total == correct:
        return True
    else:
        return False

def init_level(level):
    data = tanmatsuban_levels.load(level)
    objects = []
    for j in range(7):
        for i in range(16):
            if data[j][i] == "W":
                wall = Wall(i, j)
                objects.append(wall)
            elif data[j][i] in ["A", "B", "C", "D", "E", "F", "G", "H"]:
                block = Block(i, j, data[j][i])
                objects.append(block)
            elif data[j][i] in ["a", "b", "c", "d", "e", "f", "g", "h"]:
                button = Button(i, j, data[j][i].upper())
                objects.insert(0, button)
            elif data[j][i] in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                countdown = Countdown(i, j, int(data[j][i]))
                objects.append(countdown)
            elif data[j][i] in ["x", "X"]:
                slider = Slider(i, j, "↔")
                objects.append(slider)
            elif data[j][i] in ["y", "Y"]:
                slider = Slider(i, j, "↨")
                objects.append(slider)
            elif data[j][i] in ["z", "Z"]:
                phaser = Phaser(i, j)
                objects.insert(0, phaser)
            elif data[j][i] == "P":
                player = Player(i, j)
                objects.append(player)
    
    return [player, objects]
    
def save_locations(objects):
    locations = []
    for object in objects:
        locations.append([object.x, object.y])
    return locations
    
def rewind_time(objects, locations):
    i = 0
    for object in objects:
        if object.name == "countdown":
            if object.x != locations[i][0] or object.y != locations[i][1]:
                object.count += 1
                object.sprite = ["┌─┐", "│" + str(object.count) + "│", "└─┘"]
        object.x = locations[i][0]
        object.y = locations[i][1]
        i += 1
    return objects

def play_game(level):
    pixels = gfx.create_pixels(50, 23)
    key = None
    
    player, objects = init_level(level)
    prev_locations = save_locations(objects)
    
    draw(pixels, objects, level)
        
    while True:
        key = keylib.check()
        
        if key in ["KEY_LEFT", "KEY_RIGHT", "KEY_UP", "KEY_DOWN"]:
            prev_locations = save_locations(objects)
        if key == "KEY_LEFT" or key == "KEY_A":
            player.move(-1, 0, objects)
        elif key == "KEY_RIGHT" or key == "KEY_D":
            player.move(1, 0, objects)
        elif key == "KEY_UP" or key == "KEY_W":
            player.move(0, -1, objects)
        elif key == "KEY_DOWN" or key == "KEY_S":
            player.move(0, 1, objects)
        elif key == "KEY_R":
            objects.clear()
            player, objects = init_level(level)
            prev_locations = save_locations(objects)
        elif key == "KEY_BACK":
            objects = rewind_time(objects, prev_locations)
        elif key == "KEY_ESC":
            return False

        if key != None:
            draw(pixels, objects, level)
            if check_solution(objects):
                gfx.draw_border(pixels, 21, 9, 7, 2)
                gfx.draw_text(pixels, 22, 10, "SOLVED")
                gfx.print_display(pixels)
                time.sleep(1.8)
                for i in range(10):
                    key = keylib.check()
                return True
            key = None